"""
Convenience routine for matrix operations (function, derivative).

Author: Tsung-Han Lee (2017), henhans74716@gmail.com
        Nicola Lanata (2017), Aarhus
"""
import numpy as np
import itertools as it

#mf
lcomplex = True
if lcomplex:
  dtype_params = np.complex
else:
  dtype_params = np.float
#mf

# Fermi function at T=0
def calc_Fermi0(x,T=0.0):
    f=[]
    for xx in x:
    #    if abs(xx)<1.e-12:
    #        f.append(0.5)
    #    elif xx< -1.e-12:
    #        f.append(1)
    #    elif xx> 1.e-12: 
    #        f.append(0)
    #
    #    print (xx)
        if T>0.0:
            f.append(1./(1+np.exp((1.0/T)*xx)))
        else:
            if xx< 0.0: #1.e-12:
                f.append(1.0)
            elif xx>= 0.0: #1.e-12: 
                f.append(0.0)

    #    f.append(1./(1+np.exp(5000*xx)))
    #print (f)
    return np.array(f)


# Generic function of an Hermitian matrix
def funcMat(H, function, T=0.0, pr=False):
    from scipy.linalg import eigh
    N = H.shape[0]
    assert(H.shape[0] == H.shape[1])
    if not np.allclose(H.conj().T,H):
      print (H)
    assert(np.allclose(H.conj().T,H))
    #
    eigenvalues,U = eigh(H)
    Udagger = U.conj().T
#    if pr:
#    print 'Trafo. Matrix'
#    print U
    #
    if T>0:
        functioneigenvalues = function(eigenvalues,T=T)
    else:
        functioneigenvalues = function(eigenvalues)
    if pr:
        print (functioneigenvalues)
    functionE = np.zeros(U.shape) #,dtype=dtype_params)
    functionE[np.arange(N), np.arange(N)] = functioneigenvalues
    functionH =  np.dot(np.dot(U,functionE),Udagger)
    #print functionE
    #
    return functionH


# Create canonical basis Hermitian NxN matrices (and its transpose)
def Hermitian_list(N):
    H_list=[]
    tH_list=[]
    #print (N)
    #
    Z=np.zeros((N,N),dtype=dtype_params)
    for i in range(N):
        H=Z*1.0
        H[i,i]=1.0
        H_list.append(H*1.0)
        tH_list.append(H*1.0)
    #
    for i in range(N):
        for j in range(i+1,N):
            H=Z*1.0
            H[i,j]=1.0
            H[j,i]=1.0
            H_list.append(H/np.sqrt(2.0))
            tH_list.append(H/np.sqrt(2.0))
            H=Z*1.0
            H[i,j]=1.0j
            H[j,i]=-1.0j
            H_list.append(H/np.sqrt(2.0))
            tH_list.append(np.transpose(H)/np.sqrt(2.0))
    #
    assert(len(H_list)==N**2)
    return H_list, tH_list



# Given REAL array x and list of matrices H_list, construct
# linear combination:  H = \sum_n x_n [H_list]_n
def realHcombination(x,H_list):
    M=len(x)
    N=H_list[0].shape[0]
    assert(H_list[0].shape[0]==H_list[0].shape[1])
    assert(len(x)==len(H_list))
    #
    H=np.zeros((N,N))
    for i in range(M):
        #print (x[i])
        H = H + x[i] * H_list[i]
    #
    return H


# Given Hermitian matrix H extract components x_n with respect
# to a set H_list of Hermitian matrices
def inverse_realHcombination(H,H_list):
    N=H_list[0].shape[0]
    #H_list,tH_list=Hermitian_list(N)
    M=len(H_list)
    assert(H.shape[0]==H.shape[1])
    #assert(M==N**2)
    #
    x_list=[]
    for i in range(M):
        x_list.append(np.trace(np.dot(H_list[i],H)))
    x=np.array(x_list)
    #
    return x


# Given REAL array v=[x,y] and list of matrices H_list, construct
# linear combination:  R = \sum_n (x_n + i y_n) [H_list]_n
def complexHcombination(v,H_list):
    twiceM=len(v)
    M=twiceM//2
    N=H_list[0].shape[0]
    assert(twiceM % 2 == 0) # Checking that dimension of v is even
    assert(H_list[0].shape[0]==H_list[0].shape[1])
    assert(len(v)==2*len(H_list))
    #
    x=v[0:M]
    y=v[M:twiceM]
    H=np.zeros((N,N))
    for i in range(M):
        H = H + x[i] * H_list[i]
    for i in range(M):
        H = H + 1j*y[i] * H_list[i]
    #
    return H


# Given complex matrix H extract components x_n with respect
# to a set H_list of Hermitian matrices
def inverse_complexHcombination(H,H_list):
    N=H_list[0].shape[0]
    H_list,tH_list=Hermitian_list(N)
    M=len(H_list)
    assert(H.shape[0]==H.shape[1])
    assert(M==N**2)
    #
    xr_list=[]
    xi_list=[]
    for i in range(M):
        xr_list.append( np.real( np.trace(np.dot(H_list[i],H) ) ) )
        xi_list.append( np.imag( np.trace(np.dot(H_list[i],H) ) ) )
    xr=np.array(xr_list)
    xi=np.array(xi_list)

    return np.hstack((xr,xi))


# Define coefficients whose expansion give identity matrix
# within the notation of complexHcombination(v,H_list),
# assuming that the Hermitian matrices are all NxN.
# In other words, if:
# v = coefficients_identity(N)
# H_list,tH_list = cr.Hermitian_list(N) ,
# then:
# complexHcombination(v,H_list) is the identity NxN
def coefficients_identity(N):
    d=2*N**2
    v=np.zeros(d)
    v[0:N]=np.ones(N)
    #
    return v




# Given Hermitian matrix H extract following blocks,
# where S is sxs, and H is NxN
# -------------------------------------
# |         |                         | 
# |    S    |            V            | 
# |         |                         | 
# -------------------------------------
# |         |                         | 
# |         |                         | 
# |         |                         | 
# |  V^+    |            B            | 
# |         |                         | 
# |         |                         | 
# |         |                         | 
# ------------------------------------|
def get_blocks(H, s):
    N=H.shape[0]
    assert(H.shape[0]==H.shape[1])
    assert(s<=N)
    #
    S=H[0:s,0:s]
    B=H[s:N,s:N]
    V=H[0:s,s:N]
    Vdagger=H[s:N,0:s]
    #
    return S,B,V,Vdagger

# set the bock matrices of the above structure 
def set_blocks(S,B,V):
    s=S.shape[0]
    b=B.shape[0]
    N=s+b
    #
    H=np.zeros((N,N),dtype=dtype_params)
    H[0:s,0:s]=S
    H[s:N,s:N]=B
    H[0:s,s:N]=V
    H[s:N,0:s]=np.transpose(np.conjugate(V))
    #
    return H

# set the non-Hermitian bock matrices of the above structure 
def set_blocks_not_Hermitian(S,B,V1,V2):
    s=S.shape[0]
    b=B.shape[0]
    N=s+b
    #
    H=np.zeros((N,N),dtype=np.complex)
    H[0:s,0:s]=S
    H[s:N,s:N]=B
    H[0:s,s:N]=V1
    H[s:N,0:s]=V2
    #
    return H

# Given Hermitian matrix H extract following blocks,
# where E is 2x2 matrix for impurity, T1 is a sxs
# matrix has the same size of the unitary rotation
# matrix.
# -------------------------------------
# |       |             |             | 
# |   E   |      V1     |     V2      | 
# |       |             |             | 
# -------------------------------------
# |       |             |             | 
# |       |             |             | 
# |  V4   |      T1     |     V3      | 
# |       |             |             | 
# |       |             |             | 
# ------------------------------------
# |       |             |             | 
# |  V5   |      V6     |     T2      |
# |       |             |             | 
# ------------------------------------|
def get_blocks2(H, s):
    N=H.shape[0]
    b=N-s-2
    assert(H.shape[0]==H.shape[1])
    assert(s<=N)
    #
    E=H[0:2,0:2]
    T1=H[2:s+2,2:s+2]
    T2=H[s+2:,s+2:]
    V1=H[0:2,2:s+2]
    V2=H[0:2,s+2:]
    V3=H[2:s+2,s+2:]
    V4=H[2:s+2,0:2]
    V5=H[s+2:,0:2]
    V6=H[s+2:,2:s+2]
    #
    return E,T1,T2,V1,V2,V3,V4,V5,V6

# Set the block matrix of the above structure
def set_blocks2(E,T1,T2,V1,V2,V3,V4,V5,V6):
    s=T1.shape[0]
    b=T2.shape[0]
    N=s+b+2
    #
    H=np.zeros((N,N),dtype=np.complex)
    H[0:2,0:2]=E
    H[2:s+2,2:s+2]=T1
    H[s+2:,s+2:]=T2
    H[0:2,2:s+2]=V1
    H[0:2,s+2:]=V2
    H[2:s+2,s+2:]=V3
    H[2:s+2,0:2]=V4
    H[s+2:,0:2]=V5
    H[s+2:,2:s+2]=V6

    #
    return H


def dF(A, H, function, d_function):
    """
    Evaluate matrix derivative using Loewner Theorem. Df(A)(H) = d/dt|_t=0 f(A+tH) = f^[1](A) o H
    """
    from scipy.misc import derivative
    from numpy.linalg import eigh
    evals, evecs = eigh(A)
    #print evals
    Hbar = np.dot(np.conj(evecs).T, np.dot( H, evecs) ) #transform H to A's basis

    #print evals
    #create Loewner matrix in A's basis
    loewm = np.zeros(evecs.shape,dtype=dtype_params)
    for i in range(loewm.shape[0]):
        for j in range(loewm.shape[1]):
            if i==j:
                loewm[i,i] = d_function(evals[i])# derivative(function, evals[i], dx=1e-12)
            if i!=j:
                if evals[i] != evals[j]:
                    loewm[i,j]= ( function(evals[i]) - function(evals[j]) )/(evals[i]-evals[j])
                else:
                    loewm[i,j] = d_function(evals[i])# derivative(function, evals[i], dx=1e-12)

    # Perform the Schur product in A's basis then transform back to original basis.
    deriv = np.dot(evecs, np.dot( loewm*Hbar, np.conj(evecs).T ) )
    return deriv


def dF_numerical(A, H, function):
    """
    Evaluate matrix derivative numericaly element by element
    """
    from numpy.linalg import eigh
    t = 1e-10
    AptH = A + t * H
    
    evals, evecs = eigh(A)
    diag_A = np.dot( np.conj(evecs).T , np.dot( A, evecs ) )
    #print diag_A
    for i in range(diag_A.shape[0]):
        diag_A[i][i] = function( diag_A[i][i] )
    #print diag_A
    fA = np.dot( evecs, np.dot( diag_A , np.conj(evecs).T ) )
    
    evals, evecs = eigh(AptH)
    diag_AptH = np.dot( np.conj(evecs).T , np.dot( AptH, evecs ) )
    #print diag_AptH
    for i in range(diag_AptH.shape[0]):
        diag_AptH[i][i] = function( diag_AptH[i][i] )    
    #print diag_AptH
    fAptH = np.dot( evecs, np.dot( diag_AptH , np.conj(evecs).T ) )
    
    deriv = ( fAptH - fA )/t
    return deriv


#################################################

# Compute quasi-particle Hamtilonian
def calc_Hqp(T,Lambda,R):
    assert(Lambda.shape==R.shape)
    #
    N=T.shape[0]
    s=Lambda.shape[0]
    b=N-s
    #
    S,B,V,Vdagger=get_blocks(T,s)
    Vstar = np.dot(R,V)
    Hqp=set_blocks(Lambda,B,Vstar)
    #
    return Hqp

# Compute density matrix
def calc_C(H,T=0.0):
    from numpy.linalg import eigh
    N = H.shape[0]
    assert(H.shape[0] == H.shape[1])
    #
    #print "H in calc_C"
    #print H
    C = funcMat(H, calc_Fermi0,T)
    #
    return C


# [D(1-D)]^(0.5)
def denRm1(x):
    return (x*((1.0+0.j)-x))**(0.5)


#  [D(1-D)]^(-0.5)
def denR(x):
    return (x*((1.0+0.j)-x))**(-0.5)


#d/dD [D(1-D)]^(0.5) = (0.5-D)*[D(1-D]^(-0.5)
def ddenRm1(x):
    return ((0.5-x)/(x*((1.0+0.j)-x))**0.5)


def duplicate_in_spin_space(A):
    """
    Take  matrix acting in single-particle spinless space and
    duplicate it to act in spin space, i.e construct :math:`\tilde{A}` such that:
    
    .. math::
               \tilde{A}_{2i, 2j} = \tilde{A}_{2i+1,2j+1} = A_{ij}
    
    Parameters
    ----------
    A : NxN matrix
    
    Returns
    -------
    At : 2Nx2N matrix

    See also
    --------
    spin_symmetrize
    """
    if "Gf" in str(type(A)):
        At = type(A)(mesh = A.mesh, shape = (A.target_shape[0]*2,A.target_shape[1]*2))
        for iw in range(len(At.mesh)):
            for i,j in it.product(list(range(A.target_shape[0])), list(range(A.target_shape[1]))):
                At.data[iw, 2*i,2*j] = A.data[iw,i,j]
                At.data[iw, 2*i+1,2*j+1] = A.data[iw,i,j]
    else:
        At = np.zeros((A.shape[0]*2,A.shape[1]*2), dtype=A.dtype)
        for i,j in it.product(list(range(A.shape[0])), list(range(A.shape[1]))):
            At[2*i,2*j]=A[i,j]
            At[2*i+1,2*j+1]=A[i,j]
    return At

def spin_symmetrize(A, tol=1e-12):
    """Symmetrize spin up and dn, i.e compute matrix :math:`A^\mathrm{sym}` such that

    .. math::

      A^\mathrm{sym}_{ij} = \\frac{1}{2} (A_{2i,2j} + A_{2i+1,2j+1})

    Parameters
    ------------
    A : matrix or GfImFreq
    
    See also
    --------
    duplicate_in_spin_space
    """     
    if "Gf" in str(type(A)):
        A_sym = type(A)(mesh = A.mesh, shape = (A.target_shape[0]/2, A.target_shape[1]/2))
        for i,j in it.product(list(range(A_sym.target_shape[0])), list(range(A_sym.target_shape[1]))):
            diff = A[2*i,2*j] - A[2*i+1,2*j+1]
            for iw in range(len(diff.mesh)):
                assert (abs(diff.data[iw,0,0])<tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s"%(i,j, A[2*i, 2*j], A[2*i+1, 2*j+1])
            A_sym[i,j] = 0.5*(A[2*i,2*j] + A[2*i+1,2*j+1])
    elif (type(A)==np.ndarray and A.ndim==3) or type(A)==list:
        A_sym = np.zeros((A.shape[0],A.shape[1]/2,A.shape[2]/2), dtype = A.dtype)
        for iw in range(len(A)):
            tmp = np.zeros((A[iw].shape[0]/2,A[iw].shape[1]/2), dtype = A.dtype)
            for i,j in it.product(list(range(tmp.shape[0])), list(range(tmp.shape[1]))):
                diff = A[iw,2*i,2*j] - A[iw,2*i+1,2*j+1]
                assert (abs(diff)<tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s"%(i,j, A[iw,2*i, 2*j], A[iw,2*i+1, 2*j+1])
                tmp[i,j] = 0.5*(A[iw,2*i,2*j] + A[iw,2*i+1,2*j+1])
            A_sym[iw,:,:] = tmp
    else:
        A_sym = np.zeros((A.shape[0]//2,A.shape[1]//2), dtype = A.dtype)
        for i,j in it.product(list(range(A_sym.shape[0])), list(range(A_sym.shape[1]))):
            assert (abs(A[2*i,2*j] - A[2*i+1,2*j+1])<tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s"%(i,j, A[2*i, 2*j], A[2*i+1, 2*j+1])
            A_sym[i,j] = 0.5*(A[2*i,2*j] + A[2*i+1,2*j+1])
    
    return A_sym

