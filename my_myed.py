"""
Exact diagonalization code to solve the embeding Hamiltonian as described in
https://journals.aps.org/prx/abstract/10.1103/PhysRevX.5.011008.

Note: Implemented spin penalty. No symmetry implementation yet.

Author: Tsung-Han Lee (2017), henhans74716@gmail.com
"""
import numpy as np
from scipy.sparse import csr_matrix
import scipy.sparse
from scipy.sparse.linalg import eigsh
import primme
import sys
import convenience_routines as cr
np.set_printoptions(suppress=True)

lcomplex = False
if lcomplex:
  dtype_params = np.complex
else:
  dtype_params = np.float


def build_fermion_op(no):
    '''
    Build fermionic operators for each spin+orbitals, alpha, with size 
    of Hilberspace A and B.
    Input:
        no: number of orbital
    Output:
        FH_list: a list of fermionic operator <A|[f^dagger_alpha]|B>
    '''
    hsize = 2**no # size of Hilbert space
    strb = '{0:0'+str(no)+'b}' # string to save binary configuration
    FH_list = []
    #print ('Hibert space size:', hsize)

    config_b_list = []
    for iocc in range(no+1):
      for iconf in range(hsize):
        config_b = strb.format(iconf)

        iocc1 = 0
        for i in range(len(config_b)):
          iocc1 += int(config_b[i])

        if iocc1 != iocc:
          continue
        else:
          config_b_list.append(config_b)


    for o in range(no): #calculate FH for each orbital
        #print ('building FH for orbital:', o)
        row = []
        col = []
        data = []
        for b in range(hsize): # col |B>
            config_b = config_b_list[b] # cofiguration corresponding to B

            if config_b[o] == '0':
                col.append(b)
                config_a = ''

                #calculate the corresponding <A|
                for i in range(no):
                    if i == o:
                        config_a += '1'
                    else:
                        config_a += config_b[i]

                # Find |A> matching to |B>
                for l in range(hsize):
                  if config_a==config_b_list[l]:
                    a = l
                row.append(a)

                #calculate the exponent for minus sign
                expo = 0
                for i in range(o):
                    expo += int(config_b[i])
                data.append((-1.)**(expo))
        #assign values into sparse matrix
        FH = csr_matrix((data, (row, col)), shape=(hsize, hsize), dtype=np.float)
        FH_list.append(FH)

    return FH_list


def build_FH_operators(FH_list, no, nqo, hsize, imp, l1s, l2p):
    # Compute 1-el. and 2-el. operators in whole Fock space and only
    # save block corresponding to half-filling to disk

    if l1s:
      imp_label = "1s"
    elif l2p:
      imp_label = "2p"
    else:
      sys.exit("Code not adapted for d-shells")

    ntoto = no+nqo
    nhalf = ntoto//2

    # Get size of Fock corresponding to half-filliing and compute offsets
    ioff = 0
    for i in range(nhalf):
      ioff += int(scipy.special.binom(ntoto,i))
    hsize = int(scipy.special.binom(ntoto,nhalf))
    iend = ioff + hsize

    #build local one-body part
    array_info = []
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(no):
        for j in range(no):
            prod = csr_matrix(FH_list[i].dot( FH_list[j].getH() ).toarray()[ioff:iend,ioff:iend])
            fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            scipy.sparse.save_npz(fname,prod)

    #build hybridization part 1
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nqo):
        for j in range(no):
            prod = csr_matrix(FH_list[j].dot( FH_list[i+no].getH() ).toarray()[ioff:iend,ioff:iend])
            fname = "physical-bath-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            scipy.sparse.save_npz(fname,prod)

    #build hybridization part 2
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nqo):
      for j in range(no):
            prod = csr_matrix(FH_list[i+no].dot( FH_list[j].getH() ).toarray()[ioff:iend,ioff:iend])
            fname = "bath-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            scipy.sparse.save_npz(fname,prod)

    #build bath part d*d^dagger
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nqo):
        for j in range(nqo):
            prod = csr_matrix(FH_list[j+no].getH().dot( FH_list[i+no] ).toarray()[ioff:iend,ioff:iend])
            fname = "bath-bath-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            scipy.sparse.save_npz(fname,prod)

    #build bath part d^dagger*d
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nqo):
        for j in range(nqo):
            prod = csr_matrix(FH_list[i+no].dot( FH_list[j+no].getH() ).toarray()[ioff:iend,ioff:iend])
            fname = "bath-bath-no-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            scipy.sparse.save_npz(fname,prod)

    #build local two-body part
    prod = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(no):
        for j in range(no):
            for k in range(no):
                for l in range(no):
                    prod = csr_matrix(FH_list[i].dot( FH_list[j].dot( FH_list[l].getH().dot(FH_list[k].getH() ) ) ).toarray()[ioff:iend,ioff:iend])
                    fname = "physical-interaction-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)+str(k)+str(l)
                    scipy.sparse.save_npz(fname,prod)

    #build spin operators:
    #build total spin operators S+
    Sp = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nhalf): #no//2):
        Sp += csr_matrix(FH_list[2*i].dot(FH_list[2*i+1].getH()).toarray()[ioff:iend,ioff:iend])
    #build total spin operators S-
    Sm = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nhalf): #no//2):
        Sm += csr_matrix(FH_list[2*i+1].dot(FH_list[2*i].getH()).toarray()[ioff:iend,ioff:iend])
    Sz = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(nhalf): #no//2):
        Sz += ( 0.5*csr_matrix(FH_list[2*i].dot(FH_list[2*i].getH()).toarray()[ioff:iend,ioff:iend]) - 0.5*csr_matrix(FH_list[2*i+1].dot(FH_list[2*i+1].getH()).toarray()[ioff:iend,ioff:iend]) )
    #build S^2 operator
    S2 = Sm.dot(Sp)+Sz.dot(Sz)+Sz
    scipy.sparse.save_npz("S2-imp"+str(imp) + imp_label,S2)


    # Build angular momentum operators
    #build raising operator L+
    Lp = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(2):
        Lp += (np.sqrt(1+(i-1)+1)*np.sqrt(1-(i-1)) * csr_matrix(FH_list[2*i+2].dot(FH_list[2*i].getH()).toarray()[ioff:iend,ioff:iend])
             + np.sqrt(1+(i-1)+1)*np.sqrt(1-(i-1)) * csr_matrix(FH_list[2*i+2+1].dot(FH_list[2*i+1].getH()).toarray()[ioff:iend,ioff:iend]))
    for i in range(2):
        Lp += (np.sqrt(1+(i-1)+1)*np.sqrt(1-(i-1)) * csr_matrix(FH_list[6+2*i+2].dot(FH_list[6+2*i].getH()).toarray()[ioff:iend,ioff:iend])
             + np.sqrt(1+(i-1)+1)*np.sqrt(1-(i-1)) * csr_matrix(FH_list[6+2*i+2+1].dot(FH_list[6+2*i+1].getH()).toarray()[ioff:iend,ioff:iend]))

    #build lowering operator L-
    Lm = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(2):
        Lm += (np.sqrt(1-(i)+1)*np.sqrt(1+(i)) * csr_matrix(FH_list[2*i].dot(FH_list[2*i+2].getH()).toarray()[ioff:iend,ioff:iend])
             + np.sqrt(1-(i)+1)*np.sqrt(1+(i)) * csr_matrix(FH_list[2*i+1].dot(FH_list[2*i+2+1].getH()).toarray()[ioff:iend,ioff:iend]))
    for i in range(2):
        Lm += (np.sqrt(1-(i)+1)*np.sqrt(1+(i)) * csr_matrix(FH_list[6+2*i].dot(FH_list[6+2*i+2].getH()).toarray()[ioff:iend,ioff:iend])
             + np.sqrt(1-(i)+1)*np.sqrt(1+(i)) * csr_matrix(FH_list[6+2*i+1].dot(FH_list[6+2*i+2+1].getH()).toarray()[ioff:iend,ioff:iend]))

    Lz = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(3):
        Lz += ( (float(i)-1)*csr_matrix(FH_list[2*i].dot(FH_list[2*i].getH()).toarray()[ioff:iend,ioff:iend]) 
             + (float(i)-1.0)*csr_matrix(FH_list[2*i+1].dot(FH_list[2*i+1].getH()).toarray()[ioff:iend,ioff:iend]) )
    for i in range(3):
        Lz += ( (float(i)-1)*csr_matrix(FH_list[2*i+6].dot(FH_list[2*i+6].getH()).toarray()[ioff:iend,ioff:iend]) 
             + (float(i)-1.0)*csr_matrix(FH_list[2*i+1+6].dot(FH_list[2*i+1+6].getH()).toarray()[ioff:iend,ioff:iend]) )



    #build L^2 operator
    L2 = Lm.dot(Lp)+Lz.dot(Lz)+Lz
    scipy.sparse.save_npz("L2-imp"+str(imp) + imp_label,L2)

    #sys.exit('After L')




def build_Hemb_matrix(D, H1E, LAMBDA, V2E, imp, l1s, l2p):
    '''
    Build Hemb matrix.
    Input:
        D: hybridization matrix
        H1E: local one-body
        LAMBDA: bath one-body
        V2E: local two-body interaction
        FH_list: fermion operator list
    Return:
        Hemb: embedding Hamiltonian
    '''

    no    = D.shape[1] 
    nqo   = D.shape[0]
    ntoto = no + nqo
    nhalf = ntoto//2

    if l1s:
      imp_label = "1s"
      hsize = int(scipy.special.binom(ntoto,nhalf))
    elif l2p:
      imp_label = "2p"
      hsize = int(scipy.special.binom(12,6))
    else:
      sys.exit("Code not adapted for d-shells")

    Hemb = csr_matrix((hsize,hsize), dtype=dtype_params)
    H1mat = csr_matrix((hsize,hsize), dtype=dtype_params)
    Dmat = csr_matrix((hsize,hsize), dtype=dtype_params)
    Dconjmat = csr_matrix((hsize,hsize), dtype=dtype_params)
    Lammat = csr_matrix((hsize,hsize), dtype=dtype_params)

    #build local one-body part
    for i in range(no):
        for j in range(no):
            fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            Hemb += H1E[i,j]*prod
            H1mat+= H1E[i,j]*prod

    #build hybridization part
    for i in range(nqo):
        for j in range(no):
            fname = "physical-bath-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            Hemb += D[i,j]*prod
            Dmat += D[i,j]*prod

            fname = "bath-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            Hemb += D[i,j].conj()*prod
            Dconjmat += D[i,j].conj()*prod

    #build bath part
    for i in range(nqo):
        for j in range(nqo):
            fname = "bath-bath-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            Hemb += LAMBDA[i,j]*prod
            Lammat += LAMBDA[i,j]*prod

    #build local two-body part
    U2loc = csr_matrix((hsize,hsize), dtype=dtype_params)
    if type(V2E) == np.ndarray:
        for i in range(no):
            for j in range(no):
                for k in range(no):
                    for l in range(no):
                        fname = "physical-interaction-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)+str(k)+str(l)
                        prod = scipy.sparse.load_npz(fname+".npz")
                        Hemb += 0.5*V2E[i,k,j,l]*prod
                        U2loc+= 0.5*V2E[i,k,j,l]*prod

    return Hemb, U2loc, H1mat



def calc_density_matrix(vec,no,nqo,imp,l1s,l2p):
    '''
    calculate density matrix.
    Input:
        FH_list: fermion operator list
        vec: ground state eigenvector
    Return:
        dm: density matrix
    '''
    if l1s:
      imp_label = "1s"
    elif l2p:
      imp_label = "2p"
    else:
      sys.exit("Code not adapted for d-shells")


    dm = np.zeros((no+nqo,no+nqo),dtype=dtype_params)
    for i in range(no):
        for j in range(no):
            fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            dm[i,j] = vec.conj().T.dot( prod.dot(vec) )

    for i in range(nqo):
        for j in range(nqo):
            fname = "bath-bath-no-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            dm[no+i,no+j] = vec.conj().T.dot( prod.dot(vec) )

    for i in range(nqo):
        for j in range(no):
            fname = "physical-bath-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            dm[j,no+i] = vec.conj().T.dot( prod.dot(vec) )

    for i in range(nqo):
        for j in range(no):
            fname = "bath-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            dm[no+i,j] = vec.conj().T.dot( prod.dot(vec) )

    return dm

def calc_double_occ(idx,vec,imp,l1s,l2p):
    '''
    calculate double occupancy at orbital idx.
    Input:
        idx: index for the orbital (also idx+1) where the double occupancy is calculated.
        FH_list: fermion operator list.
        vec: ground state eigenvector.
    Return:
        double occupancy
    '''
    if l1s:
      imp_label = "1s"
    elif l2p:
      imp_label = "2p"
    else:
      sys.exit("Code not adapted for d-shells")

    fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(idx)+str(idx)
    prod1 = scipy.sparse.load_npz(fname+".npz")
    fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(idx+1)+str(idx+1)
    prod2 = scipy.sparse.load_npz(fname+".npz")

    return vec.conj().T.dot( prod1.dot( prod2.dot( vec ) ) )


def calc_Eloc(H1E,V2E,vec,imp,l1s,l2p):
    '''
    calculate density matrix.
    Input:
        U2loc: local two-body interaction operator
        vec: ground state eigenvector
    Return:
        local two-body ground state energy
    '''
    if l1s:
      imp_label = "1s"
      hsize = int(scipy.special.binom(4,2))
    elif l2p:
      imp_label = "2p"
      hsize = int(scipy.special.binom(12,6))
    else:
      sys.exit("Code not adapted for d-shells")

    no = H1E.shape[0]
    H1mat = csr_matrix((hsize,hsize), dtype=dtype_params)
    U2loc = csr_matrix((hsize,hsize), dtype=dtype_params)

    for i in range(no):
        for j in range(no):
            fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)
            prod = scipy.sparse.load_npz(fname+".npz")
            H1mat+= H1E[i,j]*prod

    for i in range(no):
        for j in range(no):
            for k in range(no):
                for l in range(no):
                    fname = "physical-interaction-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)+str(k)+str(l)
                    prod = scipy.sparse.load_npz(fname+".npz")
                    U2loc+= 0.5*V2E[i,k,j,l]*prod

    return vec.conj().T.dot( U2loc.dot( vec ) ), vec.conj().T.dot( H1mat.dot( vec ) )




def solve_Hemb(D, H1E, LAMBDA, V2E, idx, spin_pen, orbang_penalty, imp, l1s, l2p):
    '''
    solve embeding Hamiltonian.
    Input:
        D: hybridization matrix
        H1E: local one-body
        LAMBDA: bath one-body
        V2E: local two-body interaction
        FH_list: fermion operator list
        idx: index for the orbital (also idx+1) where the double occupancy is calculated.
        spin_pen: prefactor for the S^2 penalty term.
    Return:
        density matrix
        double occupancy
    '''
    no  = D.shape[1]
    nqo = D.shape[0]
    ntoto = no + nqo
    nhalf = ntoto//2

    if l1s:
      imp_label = "1s"
      hsize = int(scipy.special.binom(ntoto,nhalf))
    elif l2p:
      imp_label = "2p"
      hsize = int(scipy.special.binom(ntoto,nhalf))
    else:
      sys.exit("Code not adapted for d-shells")

    # Build embedding Hamiltonian
    Hemb, U2loc, H1mat = build_Hemb_matrix(D, H1E, LAMBDA, V2E, imp, l1s, l2p)

    # Build number operator
    Ntot = csr_matrix((hsize,hsize), dtype=dtype_params)
    for i in range(no):
        fname = "physical-physical-imp" + str(imp) + imp_label + "_operator"+str(i)+str(i)
        prod = scipy.sparse.load_npz(fname+".npz")
        Ntot+=prod
    for i in range(nqo):
        fname = "bath-bath-no-imp" + str(imp) + imp_label + "_operator"+str(i)+str(i)
        prod = scipy.sparse.load_npz(fname+".npz")
        Ntot+=prod


    # Build S^2 operator
    S2 = csr_matrix((hsize,hsize),dtype=dtype_params)
    S2 = scipy.sparse.load_npz("S2-imp"+str(imp) + imp_label+".npz")
    Hemb += spin_pen*S2

    # Build L^2 operator
    L2 = csr_matrix((hsize,hsize),dtype=dtype_params)
    L2 = scipy.sparse.load_npz("L2-imp"+str(imp) + imp_label+".npz")
    Hemb += orbang_penalty*L2

    num_eig = 1
    ieig = 0
    vals, vecs = primme.eigsh(Hemb, num_eig, tol=1e-12, which='SA')
    #print (vals)

    dm = calc_density_matrix(vecs[:,ieig],no,nqo,imp,l1s,l2p)
    docc = []
    #docc = calc_double_occ(idx,vecs[:,ieig],imp,l1s,l2p)
    for i in range(3):
      docc.append(calc_double_occ(i*2,vecs[:,ieig],imp,l1s,l2p))

    # Sanity check 1: Did we find the correct state at half-filling?
    if l1s:
      if np.abs( (vecs[:,ieig].conj().T.dot(Ntot.dot(vecs[:,ieig]))) - nhalf) > 0.000001:
        print('WARNING: Incorrect number of 1s or 2s particles')
    elif l2p:
      if np.abs( (vecs[:,ieig].conj().T.dot(Ntot.dot(vecs[:,ieig]))) - 6.0) > 0.000001:
        print('WARNING: Incorrect number of 2p particles')

    # Sanity check 2: Did we find the singlet state?
    if ((np.abs(vecs[:,ieig].conj().T.dot( S2.dot( vecs[:,ieig] ) ))-0.00000) > 0.000001):
      for i in range(num_eig):
        print (np.abs(vecs[:,i].conj().T.dot( S2.dot( vecs[:,i] ) )))
      print('WARNING: Incrrect spin state: <S^2> > 0')

    # Sanity check 3: Did we find the L^2=0 state?
    if ((np.abs(vecs[:,ieig].conj().T.dot( L2.dot( vecs[:,ieig] ) ))-0.00000) > 0.000001):
      for i in range(num_eig):
        print (np.abs(vecs[:,i].conj().T.dot( L2.dot( vecs[:,i] ) )))
      print('WARNING: Incrrect orbital angular momentum: <L^2> > 0')



    return vecs[:,ieig], vals, dm, docc


