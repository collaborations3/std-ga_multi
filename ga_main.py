import numpy as np
import h5py
import convenience_routines as cr
import subprocess as sp
import scipy
from scipy import optimize
from scipy.linalg import eigh, expm, det
import my_myed as ed
import sys
import os
import matplotlib
matplotlib.use('TkAgg')
from pylab import *
from functools import reduce
import itertools as it
from scipy import integrate
import sys
import solver as slv



class GA:
    def __init__(self,U,nghost,nspinorb,n,lml,T=0.002,solver=None,xtol=None,eps=None,xfactor=None,l1s=False,l2p=True):
        self.nspinorb  = nspinorb                   # Physical spin orbitals
        self.norb      = self.nspinorb//2           # Spatial quasi orbs (Physical + Ghost)
        self.ntoto     = 2*self.nspinorb            # Total orbs for EH
        self.nhalf     = self.ntoto//2              # Half-filling for EH
        self.n         = n                          # Total nr. of particles

        self.U         = U                          # Interaction strength
        self.T         = T

        # Create list of Hermitian matrices (Here: Only needed for Lambda)
        self.H_list = [np.array(((1,0,0),(0,0,0),(0,0,0))),
                  np.array(((0,0,0),(0,1,0),(0,0,0))),
                  np.array(((0,0,0),(0,0,0),(0,0,1)))] #,
             #     1./np.sqrt(2)*np.array(((0,1,0),(1,0,0),(0,0,0))),
             #     1./np.sqrt(2)*np.array(((0,0,0),(0,0,1),(0,1,0))),
             #     1./np.sqrt(2)*np.array(((0,0,1),(0,0,0),(1,0,0)))]

        # Precompute operators for EH and save them on file
        imp = 0 # Only one impurity
        if l1s:
          imp_label = "1s"
        elif l2p:
          imp_label = "2p"

        if not lml:
          # Check whether we already have the operators:
          op_list = ("physical-physical-imp", "physical-bath-imp", "bath-physical-imp", "bath-bath-no-imp", "bath-bath-no-imp")
          lexist1e = True
          for op in op_list:
            for i in range(self.nspinorb):
              for j in range(self.nspinorb):
                if not lexist1e:
                  continue
                fname = op + str(imp) + imp_label + "_operator"+str(i)+str(j) + ".npz"
                lexist1e = os.path.isfile(fname)

          lexist2e = True
          for i in range(self.nspinorb):
            for j in range(self.nspinorb):
              for k in range(self.nspinorb):
                for l in range(self.nspinorb):
                  if not lexist2e:
                    continue
                  fname = "physical-interaction-imp" + str(imp) + imp_label + "_operator"+str(i)+str(j)+str(k)+str(l) + ".npz"
                  lexist2e = os.path.isfile(fname)
               
          lexistS2 = os.path.isfile("S2-imp"+str(imp) + imp_label + ".npz")
          lexistL2 = os.path.isfile("L2-imp"+str(imp) + imp_label + ".npz")

          lcreate = not (lexist1e and lexist2e and lexistS2 and lexistL2) 


          if lcreate:
            print ("No/Not all operators found: Creating them")
            self.hsize = np.math.factorial(self.ntoto)//(np.math.factorial(self.nhalf)*(np.math.factorial(self.ntoto-self.nhalf)))
            FH_list = ed.build_fermion_op(self.ntoto)
            ed.build_FH_operators(FH_list,self.nspinorb,self.nspinorb,self.hsize,0,False,True)
          else:
            print ("Found all operators")

        #sys.exit()
        # Define EH solver
        if lml:
          self.eh_solver = slv.ml_solver()
        else:
          self.eh_solver = slv.ed_solver()


        # Convergence criteria
        if xtol is not None:
            self.xtol = xtol
        else:
            self.xtol = 10**(-6)

        if eps is not None:
            self.eps = eps
        else:
            self.eps = 10**(-10)

        if xfactor is not None:
            self.xfactor = xfactor
        else:
            self.xfactor = 1

        self.options = {'xtol':   self.xtol,     # Tolerance for convergence
                        'eps':    self.eps,      # Step size
                        'factor': self.xfactor   # Initial factor of the step size
                       }
        if solver is not None:
            self.solver = solver
        else:
            self.solver = 'hybr'

        self.niter = 0 # Number of iterations


    def dos(self,x):
        D = (2.0/np.pi)*np.sqrt(1.0-x**(2.0))
        if np.abs(x)>1:
          D = 0.0
        return D    

    
    def root_mu(self,mu):
        def integrand(x,Roprod,mu,idx,jdx):
                Hqp = np.zeros((self.norb,self.norb))
                Hqp           = Roprod*x + self.Lmbda
                exp = Hqp - mu*np.eye(self.norb)

                f = self.dos(x)*cr.calc_C(exp,T=self.T)[idx,jdx]
                return f
            
        Roprod = np.zeros((self.norb,self.norb))
        Roprod = np.dot(self.R,self.R.T)

        Trace = 0
        for i in range(self.norb):
            integral, error = integrate.quad(integrand, -1, 1,args=(Roprod,mu,i,i),epsabs=0.0000001)
            Trace += integral
    
        return Trace-self.n
    

    def calc_mu(self):
        chem_pot = optimize.root_scalar(self.root_mu,bracket=(5+np.abs(self.Lmbda[0,0]) , -np.abs(self.Lmbda[0,0])-5),method="bisect",x0=0.0) #,tol=0.000000001)
    
        self.mu = chem_pot.root
        
    
    def calc_Delta(self):
        def integrand(x,Roprod,idx,jdx):
                Hqp = np.zeros((self.norb,self.norb))
                Hqp            = Roprod*x + self.Lmbda
                exp = Hqp - self.mu*np.eye(self.norb)

                f = self.dos(x)*cr.calc_C(exp,T=self.T)[idx,jdx]

                return f
    
        Roprod = np.dot(self.R,self.R.T)
    
        Delta = np.zeros((self.norb,self.norb))
        for i in range(self.norb):
            for j in range(i,self.norb):
                integral,error = integrate.quad(integrand, -1, 1,args=(Roprod,i,j))
                Delta[i,j] = integral
                Delta[j,i] = integral
    
        self.Delta = Delta

    
    def calc_D(self):
        def integrand1(x,Roprod,idx,jdx):
            Hqp            = Roprod*x + self.Lmbda - self.mu*np.eye(self.norb)

            Hk             = np.zeros((self.norb,self.norb))
            Hk[:,:]        = self.dos(x) * x


            RxDen = np.dot(self.R.T,cr.calc_C(Hqp))
            f = np.dot(Hk,RxDen)[idx,jdx]

            return f
        
        Roprod = np.dot(self.R,self.R.T)
    
        Left = np.zeros((self.norb,self.norb))
        for i in range(self.norb):
          for j in range(self.norb):
              integral,error = integrate.quad(integrand1, -1, 1,args=(Roprod,i,j))
              Left[i,i] = integral
    
        #Left = np.reshape(Left,(self.nqspo,1))
        Right = cr.funcMat(self.Delta, cr.denR) 
        self.D = np.dot(Right,Left)
    

    def calc_Lmbdac(self):
        lmbda = cr.inverse_realHcombination(self.Lmbda,self.H_list)
        lmbdac = np.zeros((len(lmbda)))
        DxR = np.dot(self.D,self.R.T)
        for imat in range(len(lmbda)):
          deriv_Delta = cr.dF(self.Delta,self.H_list[imat].T,cr.denRm1,cr.ddenRm1)
          deriv_DeltaxDR = np.trace(np.dot(DxR,deriv_Delta))
          lmbdac[imat] = -lmbda[imat] - (deriv_DeltaxDR + np.conjugate(deriv_DeltaxDR))
    
        self.Lmbdac = cr.realHcombination(lmbdac,self.H_list)
   
 
    def cart_to_sph(self):
        D = self.D[0,0]
        Lmbdac = self.Lmbdac[0,0]
        U = np.copy(self.U)

        r     = np.sqrt(U**2+Lmbdac**2+(8*D)**2)
        if not (D==0 and Lmbdac==0 and U==0):
            self.theta = np.arccos((-8*D/r)/np.sqrt((U/r)**2+(Lmbdac/r)**2+(8*D/r)**2))
        else:
            self.theta = np.pi/2.0
    
        if not (U==0 and Lmbdac==0):
            self.phi   = np.arcsin((Lmbdac/r)/np.sqrt((U/r)**2+(Lmbdac/r)**2)) # D was U before
            Uang = np.arccos((U/r)/np.sqrt((U/r)**2+(Lmbdac/r)**2))
        else:
            self.phi = 0.0
            Uang = 0.0
    
        #if (n==0.5 and U==0):
        #    phi = 0
        #    Uang = 0
    
        if not (D==0 and Lmbdac==0 and U==0):
            r = np.sqrt((U/r)**2+(Lmbdac/r)**2+(8*D/r)**2)
        else:
            r = 1.0
    
        if Uang > np.pi/2 or Uang < -np.pi/2:
            sys.exit("U is negative")
    
        if np.abs(r-1.0) > 10**(-10):
            sys.exit("r is not 1 " + str(r))
    
    
    def check_sphere(self):
        self.xdfac = 1.0
        if self.theta > np.pi/2:
            self.xdfac = -1.0
            self.theta = np.pi-self.theta
    
        if self.theta > np.pi or self.theta < 0:
            sys.exit("Invalid value for theta: Theta must be between 0 and pi corresponding to D to -D")
    
        if self.phi < -np.pi/2 or self.phi > np.pi/2:
            sys.exit("U is negative")
    


    def optimize(self,rinit=None,lambdainit=None):        
        def root_GA(xinit):
            print ("####### ITERATION #######", self.niter+1)
    
            if len(xinit)==2:
              lmbda0 = xinit[0]
              r0     = xinit[1]

              self.Lmbda = np.eye(3)*lmbda0
              self.R     = np.eye(3)*r0
            else:
              sys.exit("Code not really tested for Lambda and R not being proportional to the identity")
              lmbda0 = xinit[0:self.norb*(self.norb+1)//2]
              r0     = xinit[self.norb*(self.norb+1)//2:self.norb*(self.norb+1)//2 + self.norb*self.norb]
          
              self.R = np.reshape(r0,(self.norb,self.norb)) #np.array(r0),(self.nqspo,1))
              self.Lmbda = cr.realHcombination(lmbda0,self.H_list)
           
            print ("R")
            print (self.R)
            print ("Lambda")
            print (self.Lmbda)
 
            # Solve for chemical potential (Canonnical Ensemble)
            self.calc_mu()
        
            # Get active block of density matrix
            self.calc_Delta()
            print ("Delta")
            print (self.Delta)        

            # Compute Lagrange multipliers D
            self.calc_D()
            print ("D")
            print (self.D)
         
            # Compute Lagrange multipliers Lambda^c
            self.calc_Lmbdac()
            print ("Lambda^c")
            print (self.Lmbdac)

            # Project D, Lamba^c and U onto unit sphere
            self.cart_to_sph()
            self.check_sphere()

            # Solve EH 
            D_params = (self.D[0,0],) #self.D[1,1],self.D[2,2],self.D[0,1],self.D[1,0],self.D[0,2],self.D[2,0],self.D[1,2],self.D[2,1])
            Lc_params = (self.Lmbdac[0,0],) #self.Lmbdac[1,1],self.Lmbdac[2,2],self.Lmbdac[0,1],self.Lmbdac[0,2],self.Lmbdac[1,2])
            self.eh_solver.solve_EH(Ec_params=(0.0,),D_params=D_params,Lc_params=Lc_params,UJ_params=(self.U,xJoverU),lJoverU=True,alpha=2.5)
            self.docc = self.eh_solver.docctmp
            print ("Double occupancy:", self.docc)


            # If theta>pi/2 multiply <cd> by -1
            self.eh_solver.fdaggerc = self.eh_solver.fdaggerc*self.xdfac

            # Compute F1 and F2
            self.F1 = self.eh_solver.fdaggerc*np.eye(3) - np.dot(self.R.T,cr.funcMat(self.Delta, cr.denRm1))
            self.F2 = self.eh_solver.ffdagger*np.eye(3) - self.Delta

            print ("F1")
            print (self.F1)
            print ("F2")
            print (self.F2)
            self.Rsol = np.copy(self.R)
            self.Lambdasol = np.copy(self.Lmbda)

            # Compute norm of F1 and F2 for convergence check 
            self.F1nrm = np.linalg.norm(self.F1,'fro')
            self.F2nrm = np.linalg.norm(self.F2,'fro')
        
            self.niter += 1   
        
   
            if len(xinit)==2:
              return np.hstack((self.F1[0,0],self.F2[0,0]))
            else: 
              return np.hstack((cr.inverse_realHcombination(self.F2,self.H_list),np.reshape(self.F1,(1,9))[0]))

        if rinit is None or lambdainit is None: # Use default initial guesses for R and Lambda
            self.rinit = (1,0,0,0,1,0,0,0,1)
            self.lambdainit = (0.05,0.05,0.05,0,0,0)
        else:
            self.rinit = rinit
            self.lambdainit = lambdainit
        
        # Solve root problem
        xinit = np.hstack((lambdainit,rinit))
        opti = optimize.root(root_GA,xinit,method=self.solver,options=self.options)

        self.converged = True
        if self.F1nrm > self.xtol:
            print ("WARNING: F1 not converged", self.F1nrm)
            self.converged = False
        if self.F2nrm > self.xtol:
            print ("WARNING: F2 not converged", self.F2nrm)
            self.converged = False






lswitch = True   # Adiabatic switching
lml     = False  # Machine Learning  EH solver?


U_list = (0.0, 0.1,0.2,0.2631578947368421) #0.5,0.7)

nspinorb   = 6                 # Nr. of spin orbitals
nghost     = nspinorb          # Nr. of bath orbitals/spin
n          = 0.6 #2.5               # Number of particles

xJoverU    = 1./4.


GA_list = []     # A separate GA object for each U


icnt = 0
Z =  []
for U in U_list:

    if icnt==0 or not lswitch:
      rinit = 1.0
      lambdainit = 0.0
    else:
      rinit = GA_list[icnt-1].Rsol[0,0]
      lambdainit = GA_list[icnt-1].Lambdasol[0,0]


    # Initialize GA object
    GA_list.append(GA(U,nghost,nspinorb,n,lml,eps=10**(-13),xtol=10**(-10)))
    # Solve root problem
    GA_list[icnt].optimize(rinit,lambdainit)
    
    Z.append(GA_list[icnt].Rsol[0,0]**2)
    print ("R:",GA_list[icnt].Rsol)
    print ("Lambda:",GA_list[icnt].Lambdasol)
    #print ("Double occupancy:",GA_list[icnt].docc)
    print ("Z:",Z[icnt])
    if GA_list[icnt].converged:
      print ("GA Converged for U=",U)
    else:
      print ("GA NOT converged for U=",U)

    icnt += 1

plt.title("Fixed J/U="+str(xJoverU)+"; n="+str(n))
plt.plot(U_list,Z)
plt.xlabel("U")
plt.ylabel("Z")
plt.ylim(0,1.1)
plt.show()





