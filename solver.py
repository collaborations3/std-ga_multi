import numpy as np
import convenience_routines as cr 
import subprocess as sp
import scipy
import my_myed as ed
import sys
import os
import itertools as it 
import coulomb_matrix as cls

np.seterr(divide='raise')
np.set_printoptions(precision=8,suppress=True)  



class ed_solver:
  def __init__(self,l1s=False,l2p=True,lghost=False,nghost=0):
    assert(l1s!=l2p)
    assert(nghost*int(lghost)==nghost)

    self.l1s = l1s
    self.l2p = l2p
    self.lghost = lghost
    self.nghost = nghost

    if l1s:
      self.nspinorb  = 2                     # Real (physical) spin orbitals
    elif l2p:
      self.nspinorb  = 6
    else:
      sys.exit("Wait, Bro!")
    
    self.nquasiorb = self.nspinorb + self.nghost      # Quasi-orbitals (bath) for ghost-GA
    self.ntoto     = self.nspinorb + self.nquasiorb   # Total number of orbitals: Bath + Physical + Ghost where dim(Bath) = dim(Physical)
    self.nhalf     = self.ntoto//2                    # Number of particles at half-filling


    self.hsize = np.math.factorial(self.ntoto)//(np.math.factorial(self.nhalf)*(np.math.factorial(self.ntoto-self.nhalf)))

    # Create list of Fermionic creation/annhilation operators
    #self.FH_list = ed.build_fermion_op(self.nspinorb+self.nquasiorb)

    # Create "strings" of creation and annhilation operators => 1-el. and 2-el. operators
    #ed.build_FH_operators(self.FH_list,self.nspinorb,self.nquasiorb,self.hsize,0,self.l1s,self.l2p)

    self.spin_pen = 1000.0
    self.orbang_penalty = 1000.0



  def solve_EH(self,Ec_params=None,D_params=None,Lc_params=None,UJ_params=None,lJoverU=True,alpha=None): 
    
    if Ec_params==None or D_params==None or Lc_params==None or UJ_params==None:
      sys.exit("Please specify, Ec, D, Lambda^c and U,J")

    if len(UJ_params)!=2:
      sys.exit("Need U and J OR U and J/U")

    if lJoverU:
      U_hubb = UJ_params[0] 
      J_hund = U_hubb*UJ_params[1]
    #  U_hubb = UJ_params[0]
    #  J_hund = UJ_params[0]/UJ_params[1]
    else:
      U_hubb = UJ_params[0]
      J_hund = UJ_params[1]

    if alpha==None:
      self.alpha = 2.5
    else:
      self.alpha = alpha

    if self.alpha!=0.0 and len(Lc_params)>1:
      sys.exit("Shift not adapted for non-unity case")

    # Set up Ec matrix (physical part of EH) and extend in spin space
    Ec = np.zeros((3,3))
    if len(Ec_params)==1:
      # Proportional to identity
      Ec = np.eye(3)*Ec_params[0]
    elif len(Ec_params)==3:
      # Diagonal but different entries on diagonal
      Ec[0,0] = Ec_params[0]
      Ec[1,1] = Ec_params[1]
      Ec[2,2] = Ec_params[2]
    elif len(Ec_params)==6:
      # General symmetric matrix
      Ec[0,0] = Ec_params[0]
      Ec[1,1] = Ec_params[1]
      Ec[2,2] = Ec_params[2]
      Ec[0,1] = Ec[1,0] = Ec_params[3]
      Ec[0,2] = Ec[2,0] = Ec_params[4]
      Ec[1,2] = Ec[2,1] = Ec_params[5]
    else:
      sys.exit("Ec needs to be symmetric: Either 1, 3 or 6 independent parameters")

    hemb1epp = cr.duplicate_in_spin_space(np.array(((Ec))))


    # Set up Lambda^c matrix (bath part of EH) and extend in spin space
    Lc = np.zeros((3,3))
    if len(Lc_params)==1:
      # Proportional to identity
      Lc = np.eye(3)*Lc_params[0]
    elif len(Lc_params)==3:
      # Diagonal but different entries on diagonal
      Lc[0,0] = Lc_params[0]
      Lc[1,1] = Lc_params[1]
      Lc[2,2] = Lc_params[2]
    elif len(Lc_params)==6:
      # General symmetric matrix
      Lc[0,0] = Lc_params[0]
      Lc[1,1] = Lc_params[1]
      Lc[2,2] = Lc_params[2]
      Lc[0,1] = Lc[1,0] = Lc_params[3]
      Lc[0,2] = Lc[2,0] = Lc_params[4]
      Lc[1,2] = Lc[2,1] = Lc_params[5]
    else:
      sys.exit("Lambdac needs to be symmetric: Either 1, 3 or 6 independent parameters")


    hemb1ebb = cr.duplicate_in_spin_space(np.array(((Lc)))-np.eye(3)*self.alpha*U_hubb)


    # Set up D matrix (physical-bath coupling  of EH) and extend in spin space
    D = np.zeros((3,3))
    if len(D_params)==1:
      # Proportional to identity
      D = np.eye(3)*D_params[0]
    elif len(D_params)==3:
      # Diagonal but different entries on diagonal
      D[0,0] = D_params[0]
      D[1,1] = D_params[1]
      D[2,2] = D_params[2]
    elif len(D_params)==6:
      # General symmetric matrix
      D[0,0] = D_params[0]
      D[1,1] = D_params[1]
      D[2,2] = D_params[2]
      D[0,1] = D[1,0] = D_params[3]
      D[0,2] = D[2,0] = D_params[4]
      D[1,2] = D[2,1] = D_params[5]
    elif len(D_params)==9:
      D[0,0] = D_params[0]
      D[1,1] = D_params[1]
      D[2,2] = D_params[2]
      D[0,1] = D_params[3]
      D[1,0] = D_params[4]
      D[0,2] = D_params[5]
      D[2,0] = D_params[6]
      D[1,2] = D_params[7]
      D[2,1] = D_params[8]
    else:
      sys.exit("Please, either specify diagonal, upper triangle or all elements of D")

    hemb1epb = cr.duplicate_in_spin_space(np.array(((D))))

 

    # Determine Coulomb matrix from U and J
    hloc2e =  cls.coulomb_matrix_slater.from_uj(1, U_hubb, J_hund)
    hloc2e.evaluate()


    # Expand Coulomb matrix in spin space
    hemb2epp = np.zeros((self.nspinorb,self.nspinorb,self.nspinorb,self.nspinorb))
    for i in range(self.nspinorb//2):
      for j in range(self.nspinorb//2):
        for k in range(self.nspinorb//2):
          for l in range(self.nspinorb//2):
            if i==j and j==k and k==l:
              hemb2epp[2*i+1,2*j+1,2*k,2*l]     = hloc2e._v[i,j,k,l]       # (down down | up up)
              hemb2epp[2*i,2*j,2*k+1,2*l+1]     = hloc2e._v[i,j,k,l]       # (up up | down down)
            else:
              hemb2epp[2*i+1,2*j+1,2*k,2*l]     = hloc2e._v[i,j,k,l]       # (down down | up up)
              hemb2epp[2*i,2*j,2*k+1,2*l+1]     = hloc2e._v[i,j,k,l]       # (up up | down down)
              hemb2epp[2*i,2*j,2*k,2*l]         = hloc2e._v[i,j,k,l]       # (up up | up up)
              hemb2epp[2*i+1,2*j+1,2*k+1,2*l+1] = hloc2e._v[i,j,k,l]       # (down down | down down) 

    # Solve embedding Hamiltonian via exact diagonalization
    Phi, evals, denMat, self.docctmp = ed.solve_Hemb(hemb1epb,hemb1epp,hemb1ebb,hemb2epp,0,self.spin_pen,self.orbang_penalty,0,self.l1s,self.l2p)
  

    # Get "physical/bath" coupling block of density matrix
    self.fdaggerc = cr.spin_symmetrize(cr.get_blocks(denMat,self.nspinorb)[2],tol=10**(-8))
    
    # Get bath block of density matrix
    ffdagger1 = np.copy(denMat[self.nspinorb:,self.nspinorb:])
    # d d^dagger = 1 - d^dagger d
    self.ffdagger = cr.spin_symmetrize((np.eye(self.nquasiorb) - ffdagger1),tol=10**(-8))
 
    self.trace = np.trace(np.eye(self.nquasiorb) - ffdagger1)

    if len(D_params)==1 and len(Lc_params)==1 and len(Ec_params)==1:
      self.fdaggerc = self.fdaggerc[0,0]
      self.ffdagger = self.ffdagger[0,0]
    elif len(D_params)<=3 and len(Lc_params)<=3 and len(Ec_params)<=3:
      self.fdaggerc = (self.fdaggerc[0,0],self.fdaggerc[1,1],self.fdaggerc[2,2])
      self.ffdagger = (self.ffdagger[0,0],self.ffdagger[1,1],self.ffdagger[2,2])




class ml_solver(ed_solver):
  def solve_EH(self,phi,theta,xJoverU):
    self.fdaggerc, self.ffdagger = midas.get_result(phi,theta,xJoverU)






