import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import convenience_routines as cr
import subprocess as sp
import scipy
import my_myed as ed
import sys
import os
import itertools as it
import coulomb_matrix as cls
from solver import *
 
np.seterr(divide='raise')
np.set_printoptions(precision=8,suppress=True)



# Initialize EH solver object (VERY IMPORTANT TO DO THIS OUTSIDE ANY LOOP)
solver = eh_solver()

# Fix U/J ratio such that we can visualize the results
UoverJ = 2.0
r      = 1.0


# Generate Training data
def gen_train_data(phi_mesh,theta_mesh):

    cd = []
    dd = []

    for phi,theta in zip(phi_mesh.ravel(),theta_mesh.ravel()):
        D = -(1/8)*r*np.cos(theta)
        U = np.sin(theta)*np.cos(phi)
        Lambdac = np.sin(theta)*np.sin(phi)



        Ec = 0.0


        # Solve embedding problem
        solver.solve_EH(Ec_params=(Ec,),D_params=(D,),Lc_params=(Lambdac,),UJ_params=(U,UoverJ))

        cd.append(solver.fdaggerc)
        dd.append(solver.ffdagger)
  


    cd_mesh = np.reshape(cd,((phi_mesh.shape[0],phi_mesh.shape[0])))
    dd_mesh = np.reshape(dd,((phi_mesh.shape[0],phi_mesh.shape[0])))


    return cd_mesh, dd_mesh


theta=np.linspace(0,np.pi/2.0,20)
phi=np.linspace(-np.pi/2.0,np.pi/2.0,20)
theta, phi = np.meshgrid(theta, phi)


#mf Only for checking stairs (theta=pi/2)
#theta_line = np.pi/2.0
#phi_line = np.linspace(-np.pi/2.0,np.pi/2,200)
#r = 1.0
#solver = eh_solver()
#
#dd_line = []
#cd_line = []
#trace   = []
#Lc = np.linspace(-6.0,2.0,200)
#for i in phi_line:
#  D = -(1/8)*r*np.cos(theta_line)
#  U = np.sin(theta_line)*np.cos(i)
#  Lambdac = np.sin(theta_line)*np.sin(i) #-2.5*U
#
#  solver.solve_EH(Ec_params=(0.0,),D_params=(D,),Lc_params=(Lambdac,),UJ_params=(U,0.0),lUoverJ=False,alpha=2.5)
#
#  if (np.abs(solver.trace-6.0*solver.ffdagger)>0.00001):
#    sys.exit("WARNING: NON-DIAGONAL DENSITY MATRIX")
#
#
#  cd_line.append(solver.fdaggerc)
#  dd_line.append(solver.ffdagger*6.0)
#  trace.append(solver.trace)
#
#plt.plot(phi_line,dd_line) #phi_line,dd_line)
#plt.show()
#sys.exit()
#mf Only for checking stairs

cd_mesh, dd_mesh = gen_train_data(phi,theta)




# Plot ED g2
fig = plt.figure(5)
ax = fig.gca(projection='3d')
ax.set_title("ED <cd>")

surf = ax.plot_surface(theta, phi, cd_mesh, cmap=cm.coolwarm, linewidth=0)

angle=150
ax.view_init(45, angle)

plt.savefig('cd.png')



# Plot ED g1
fig = plt.figure(6)
ax = fig.gca(projection='3d')
ax.set_title("ED <dd>")

surf = ax.plot_surface(theta, phi, dd_mesh, cmap=cm.coolwarm, linewidth=0)

angle=150
ax.view_init(45, angle)

plt.savefig('dd.png')


plt.show()

